# Subdomain Modules

This project is for providing  an environment where subdomains can be utilised. 

For many projects you will require a frontend, backend approach and this project provides that with minimal code.
```
http://example.com          -> Frontend
http://backend.example.com  -> Backend (Possibly a admin panel.)
```

Services are shared between all modules .

When using the Http Interface some modules are preloaded as you can see below:
```
Dispatcher - Responsible for setting current controller path based on module.
Session    - Responsible for setting the session to be domain wide *.example.com
View       - Responsible for setting correct view path based on current module.
```

The main project file is used as a gateway to direct Http/Cli requests accordingly. The main task could be expanded to dynamically load other tasks from a database.

# Example Cron

```
*/5 * * * * cd /var/www/html/Public && php index.php
```

By default if no arguments are passed then System/Tasks/MainTask->mainAction() will be invoked.

<?php

use \Phalcon\Loader;
use \Phalcon\Config\Adapter\Ini;
use \System\Interfaces\Cli;
use \System\Interfaces\Web;

try {
    /* Change current working dir. */
    chdir('..');

    /* Configure autoloader. */
    $loader = new Loader();
    $loader->registerNamespaces(['System' => 'System']);
    $loader->register();

    /* Load configuration. */
    $config = new Ini('Config.ini');

    /* Execute app. */
    $method = php_sapi_name();
    if ($method === 'cli') {
        $interface = new Cli($config);
        $interface->exec($argc, $argv);
    } else {
        $interface = new Web($config);
        $interface->exec();
    }
} catch (Exception $e) {
    printf("An error occurred: %s\n", $e->getMessage());
}

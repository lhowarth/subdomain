<?php

namespace System\Interfaces;
use \Phalcon\Config\Adapter\Ini;
use \Phalcon\Cli\Console;
use \Phalcon\Di\FactoryDefault\Cli as FactoryDefault;

/**
 * Cli Interface.
 */
class Cli
{
    /**
     * Config.
     *
     * @param \Phalcon\Config\Adapter\Ini $config
     */
    protected $config;

    /**
     * Init.
     *
     * @param  \Phalcon\Config\Adapter\Ini $config
     * @return void
     */
    public function __construct(Ini $config)
    {
        $this->config = $config;
    }

    /**
     * Execute.
     *
     * @param  integer $argc
     * @param  array   $argv
     * @return void
     */
    public function exec(int $argc, array $argv) : void
    {
        /* Register services. */
        $services = new FactoryDefault();

        /* Register config. */
        $services->set('config', $this->config);

        /* Determine route. */
        $args = [];
        if ($argc > 1) {
            for ($arg = 1; $arg < $argc; $arg++) {
                if ($arg === 1) {
                    $args['task'] = sprintf('System\Tasks\%s', ucfirst($argv[1]));
                } elseif ($arg == 2) {
                    $args['action'] = $argv[2];
                } else {
                    $args['params'][] = $argv[$arg];
                }
            }
        } else {
            $args['task'] = 'System\Tasks\Main';
        }

        /* Execute. */
        $console = new Console();
        $console->setDi($services);
        $console->handle($args);
    }
}

<?php

namespace System\Interfaces;
use \Phalcon\Di\FactoryDefault;
use \Phalcon\Mvc\Application;
use \Phalcon\Mvc\Dispatcher;
use \Phalcon\Events\Manager;
use \Phalcon\Mvc\View;

/**
 * Web Interface.
 */
class Web
{
    /**
     * Init.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute.
     *
     * @return void
     */
    public function exec() : void
    {
        /* Determine module. */
        $pieces = explode('.', $_SERVER['HTTP_HOST'], 2);
        $module = ucfirst($pieces[0]);
        if (! is_dir("System/Controllers/$module")) {
            $module = 'Frontend';
        }

        /* Register services. */
        $services = new FactoryDefault();

        /* Register dispatcher. */
        $services->set('dispatcher', function() use ($module) {
            $eventsManager = new Manager();
            $eventsManager->attach('dispatch:beforeException', function($event, $dispatcher, $exception) {
                $dispatcher->forward(['controller' => 'error', 'action' => 'error']);
                return false;
            });
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace("System\\Controllers\\$module");
            $dispatcher->setEventsManager($eventsManager);
            return $dispatcher;
        });

        /* Register view. */
        $services->set('view', function() use ($module) {
            $view = new View();
            $view->setViewsDir("System/Views/$module/");
            return $view;
        });

        /* Execute application. */
        $application = new Application($services);
        $application->handle($_SERVER['REQUEST_URI'])
                    ->send();
    }
}

<?php

namespace System\Tasks;
use \Phalcon\Cli\Task;

/**
 * MainTask.
 */
class MainTask extends Task
{
    /**
     * Main.
     *
     * @return void.
     */
    public function mainAction() : void
    {
        printf("MainTask->mainAction() loaded.\n");
    }
}
